
.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

Simulation tools
================

RASCIL
------

The Radio Astronomy Simulation, Calibration and Imaging Library (RASCIL)
expresses radio interferometry calibration and imaging algorithms using
Python and numpy.

- See the `RASCIL documentation <https://timcornwell.gitlab.io/rascil/>`_
- See the `RASCIL repository <https://github.com/SKA-ScienceDataProcessor/rascil/>`_


OSKAR
-----

The OSKAR package has been designed to produce simulated visibility data from
radio telescopes containing aperture arrays.
The software is written mainly in C and offers GPU acceleration using
CUDA or OpenCL. The Python bindings to OSKAR make it easy to run simulations
using Python scripts.

- See the `OSKAR Python documentation <https://fdulwich.github.io/oskarpy-doc/>`_
- See the `OSKAR repository <https://github.com/OxfordSKA/OSKAR/>`_
